# Helm Chart for IoT Device Indexing (IDI)

## Purpose 

The purpose of this [Helm](https://helm.sh) chart is to offer an easy way to deploy, on [Kubernetes](https://kubernetes.io), the [Orion Context Broker]((https://fiware-orion.readthedocs.io/en/master/)) and [IoT Agent](https://fiware-iotagent-json.readthedocs.io/en/latest/) components of the IDI module, as well as any additional required ones ([Mosquitto](https://mosquitto.org/) and [RabbitMQ](https://www.rabbitmq.com/) brokers).

## Structure 

This Helm chart consists of one main Helm chart (Orion) and four subcharts (MongoDB, IoT Agent, Mosquitto, RabbitMQ).

### Orion Context Broker chart

The Orion Context Broker chart is responsible for configuring and deploying the Orion Context Broker component.

#### Source Code
* <https://github.com/telefonicaid/fiware-orion>
* <https://github.com/FIWARE/context.Orion-LD>

#### Helm chart
* <https://github.com/FIWARE/helm-charts/tree/main/charts/orion>

---

### MongoDB chart

The MongoDB chart is responsible for configuring and deploying a MongoDB instance, required by both the Orion Context Broker and IoT Agent components.

#### Source Code
* <https://github.com/bitnami/bitnami-docker-mongodb>

#### Helm chart
* <https://github.com/bitnami/charts/tree/master/bitnami/mongodb>

---

### IoT Agent chart

The IoT Agent chart is responsible for configuring and deploying the IoT Agent component. 

#### Source Code
* <https://github.com/telefonicaid/iotagent-json>

#### Helm chart
* <https://github.com/FIWARE/helm-charts/tree/main/charts/iotagent-json>

---

### Mosquitto Broker chart

The Mosquitto Broker chart is responsible for configuring and deploying a Mosquitto instance. Mosquitto is used by IoT Agent to allow for MQTT communication. 

#### Source Code
* <https://github.com/eclipse/mosquitto>

#### Helm chart
* <https://github.com/k8s-at-home/charts/tree/master/charts/stable/mosquitto>

---

### RabbitMQ broker chart

The RabbitMQ broker chart chart is responsible for configuring and deploying a RabbitMQ instance. RabbitMQ is used by IoT Agent to allow for AMQP communication. 

#### Source Code
* <https://github.com/bitnami/bitnami-docker-rabbitmq>

#### Helm chart
* <https://github.com/bitnami/charts/tree/master/bitnami/rabbitmq>


## Historical Data Registry 

Part of the IDI is the Historical Data Registry. While it is not part of the present Helm chart, its deployment is fairly simple, as all the necesary components are gathered in a single Kubernetes Manifest. 

The Historical Data Registry is responsible for holding all the changes in the data of a device, as Orion does not offer persistency for time-series data. 

### Parameters
| Name | Description | Value |
|  --- |     ---     |  ---  |
| `env.dbUsername` | The username of the Historic Data Registry | `user` |
| `env.dbPassword` | The password of the Historic Data Registry | `password` |
| `env.dbName` | The name of the Historic Data Registry database | `db` |
| `persistence.storageSize` | The size of the Historic Data Registry volume | `db` |
| `service.type` | The type of the Historic Data Registry service | `ClusterIP` |
| `service.port` | The port of the Historic Data Registry service | `8080` |
| `deployment.replicas` | The number of replicas of the Historic Data Registry | `1` |
| `deployment.image` | The image of the Historic Data Registry | `iotngin/device_indexing_registry:v0.1.3` |
| `mongoDb.replicas` | The number of replicas of the Historic Data Registry MongoDB database | `1` |
| `mongoDb.image` | The image of the Historic Data Registry MongoDB database | `mongo:5.0.6` |

## Install and upgrade

In order to install the chart, run the following command: 

```bash
helm install -f values.yaml . 
```

In order to upgrade your installed chart, in the case of changes in `values.yaml`, run the following command: 

```bash
helm upgrade --reuse-values orion -f values.yaml . 
```